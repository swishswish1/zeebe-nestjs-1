// app.controller.ts
import { Controller, Inject } from '@nestjs/common';
import { ZBClient } from 'zeebe-node';
import { ZEEBE_CONNECTION_PROVIDER, ZeebeWorker, ZeebeServer } from '@payk/nestjs-zeebe';
import { AppService } from '../services/app.service';

@Controller()
export class AppController {
  responses = new Map<string, Response>();

  constructor(
    @Inject(ZEEBE_CONNECTION_PROVIDER) private readonly zbClient: ZBClient,
    private readonly zeebeServer: ZeebeServer,
    private readonly appService: AppService,
  ) { }

  // Subscribe to events of type 'task-1' and
  //   create a worker with the options as passed below (zeebe-node ZBWorkerOptions)
  @ZeebeWorker('task-1', { maxJobsToActivate: 10, timeout: 300 })
  task1(job, complete) {
    console.log('task-1 -> Task variables', job.variables);

    // Task worker business logic
    const result = '1';

    const variableUpdate = {
      tracer: 'task-1',
      status: 'ok',
      result,
    };

    complete.success(variableUpdate);
  }
}
